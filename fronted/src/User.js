// User.js
import React from "react";

const UsersList = (props) => {
  return (
    <div>
      <h1>Salut a toi</h1>
      <ul>
        {props.users.map((user) => (
          <li key={user._id}>
            {user.username}
            <button onClick={() => props.onDelete(user._id)}>Delete</button>
            <button onClick={() => props.onSetToUpdate(user._id, user.username)}>Update</button>
            {props.userIdToUpdate === user._id && (
              <>
                <input
                  type="text"
                  value={props.updatedUsername}
                  onChange={(e) => props.onUpdatedUsernameChange(e)}
                />
                <button onClick={() => props.onUpdate(user._id, props.updatedUsername)}>Save</button>
                <button onClick={() => props.onCancelUpdate()}>Annuler</button> {/* Bouton Annuler */}
              </>
            )}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default UsersList;
