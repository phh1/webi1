// CreateUser.js
import React, { Component } from "react";
import axios from "axios";
import UsersList from "./User";







export default class CreateUser extends Component {
  state = {
    username: "",
    users: [],
    updatedUsername: "",
    userIdToUpdate: null,
  };

  componentDidMount() {
    this.fetchUsers();
  }

  fetchUsers = () => {
    axios.get("http://localhost:4000/users/read")
      .then((res) => {
        const users = res.data;
        this.setState({ users });
      })
      .catch((error) => {
        console.log("There is an error fetching users:", error);
      });
  };

  handleUpdatedUsernameChange = (e) => {
    this.setState({
      updatedUsername: e.target.value,
    });
  };

  handleUsernameChange = (e) => {
    this.setState({
      username: e.target.value,
    });
  };

  onSubmit = (e) => {
    e.preventDefault();

    const user = {
      username: this.state.username,
    };

    axios.post("http://localhost:4000/users/add", user)
      .then(() => {
        this.fetchUsers();
        this.setState({
          username: "",
        });
      })
      .catch((error) => {
        console.log("There is an error adding user:", error);
      });
  };

  handleDelete = (userId) => {
    axios.delete(`http://localhost:4000/users/delete/${userId}`)
      .then(() => {
        this.fetchUsers();
      })
      .catch((error) => {
        console.log("There is an error deleting user:", error);
      });
  };

  handleUpdate = (userId, updatedUsername) => {
    axios.put(`http://localhost:4000/users/update/${userId}`, {
      username: updatedUsername,
    })
      .then(() => {
        this.fetchUsers();
        this.setState({
          updatedUsername: "",
          userIdToUpdate: null,
        });
      })
      .catch((error) => {
        console.log("There is an error updating user:", error);
      });
  };

  handleSetToUpdate = (userId, username) => {
    this.setState({
      updatedUsername: username,
      userIdToUpdate: userId,
    });
  };

  handleCancelUpdate = () => {
    this.setState({
      updatedUsername: "",
      userIdToUpdate: null,
    });
  };







  render() {
    const { username, users, updatedUsername, userIdToUpdate } = this.state;

    return (
      

    


      <div>
        <h3>Create New User</h3>

    <h2> c'est un test</h2>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label>Username: </label>
            <input
              type="text"
              required
              className="form-control"
              value={username}
              onChange={this.handleUsernameChange}
            />
  
          </div>




          <div className="form-group">
            <input
              type="submit"
              value="Create User"
              className="btn btn-primary"
            />
          </div>
        </form>
        <div className="App">
      
      
    </div>
        <UsersList
          users={users}
          onDelete={this.handleDelete}
          onUpdate={this.handleUpdate}
          onSetToUpdate={this.handleSetToUpdate}
          onCancelUpdate={this.handleCancelUpdate} 
          updatedUsername={updatedUsername}
          userIdToUpdate={userIdToUpdate}
          onUpdatedUsernameChange={this.handleUpdatedUsernameChange}
        />
    
              </div>
              

      
      
      

      
    );
  }
}
