const mongoose = require("mongoose");


const postMessage = mongoose.Schema(

    {

        message: {
            type: String,
            required: true,
        },
        author: {
            type: String,
            required: true,

        },
      
    },
    {
        timestamps: true,
    }

);


module.exports = mongoose.model("postMessage" , postMessage);
