const mongoose = require("mongoose");


const postProduit = new mongoose.Schema({
    nom: { type: String, required: true },
    prix: { type: Number, required: true },
    dispo: { type: Boolean, required: true }
    
    
  });


module.exports = mongoose.model("postProduit" , postProduit);

