const express= require('express');
const Produit = require("../models/produits.model");
const mongoose = require('mongoose');
const routerProduit = express.Router();















routerProduit.get("/read", (req, res) => {
    Produit.find().then((err, data) => {
      if (err) {
        console.log(err);
      } else {
        console.log('exelent');
      }
    });
  });



routerProduit.post("/add",(req, res)=> {
    const book = new Produit;
    book.nom = req.body.nom;
    book.prix = req.body.prix;
    book.dispo=req.body.dispo;

    book.save().then(() => {
      console.log('livre enregistré');
    });

  });




  
routerProduit.delete("/delete/:id", (req, res)=> {
    console.log('supprimer', req.params.id)
  
    Produit.findOneAndDelete({
      _id: req.params.id
    }).exec()
      .then((message) => res.json())
      .catch((err) => res.json())
  
  })
  
routerProduit.put('/update/:id', (req, res)=> {
    console.log('mise à jour', req.params.id)
  
    Produit.findOneAndUpdate({
      _id: req.params.id
    },
      { $set: req.body },
      { new: true }
    ).exec()
      .then((message) => res.json())
      .catch((err) => res.json())
  
  
  
  
  })
  
  
  
  

  module.exports= routerProduit;