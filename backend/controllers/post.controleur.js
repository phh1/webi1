const postModels = require("../models/post.models");
const PostModel = require ( "../models/post.models");
const { param } = require("../routes/post.routes");

module.exports.getPost = async(req , res) =>{
    const post = await postModels.find();
    res.status(200).json(posts)
}
module.exports.setPosts = async (req,res) => {
    if(!req.body.message) {
        res.status(400).json({message: "merci d'ajouter un message "});
    }

    const post = await postModels.create({
        message: req.body.message,
        author:req.body.author,
    });
    res.status(200).json(post);
};

module.exports.editPost = async (req, res) => {
    const post = await PostModels.findById(req.params.id);
    if (!post){
        res.status(400).json({message: "ce post n'existe pas"});
    }
    const updatePost= await PostModels.findByIdandUpdate(
        post ,
        req.body,
        {new: true}
    )

    res.status(200).json(updatePost);

};
module.exports.deletePost = async (req,res)=> {
    const post=await PostModels.findById(req.params.id);
if (!post){
    res.status(400).json({message: "ce post n'existe pas"});
}
await post.remove();
res.status(200).json("Message supprimé " + req.params.id);

};